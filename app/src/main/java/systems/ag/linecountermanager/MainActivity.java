package systems.ag.linecountermanager;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import systems.ag.linecountermanager.db.DatabaseHelper;
import systems.ag.linecountermanager.fragment.LineFragment;
import systems.ag.linecountermanager.model.WorkUnit;
import systems.ag.linecountermanager.model.WorkUnitFragmentListener;

public class MainActivity extends FragmentActivity implements WorkUnitFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void itemClicked(WorkUnit workUnit) {
        Toast.makeText(this, workUnit.getName(), Toast.LENGTH_SHORT).show();
        LineFragment positions = new LineFragment();
        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        positions.setWorkUnit(workUnit);
        ft.replace(R.id.position_container, positions);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
