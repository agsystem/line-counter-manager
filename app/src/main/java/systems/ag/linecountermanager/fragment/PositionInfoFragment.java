package systems.ag.linecountermanager.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import systems.ag.linecountermanager.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PositionInfoFragment extends Fragment {


    public PositionInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_position_info, container, false);
    }

}
