package systems.ag.linecountermanager.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.util.HashMap;
import java.util.Map;

import systems.ag.linecountermanager.R;
import systems.ag.linecountermanager.dao.CounterPositionFacade;
import systems.ag.linecountermanager.model.CounterPosition;
import systems.ag.linecountermanager.model.WorkUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class LineFragment extends Fragment {

    private WorkUnit workUnit;


    public LineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_line, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        if(view != null) {
            GridView positionsView = (GridView) view.findViewById(R.id.positionsView);
            positionsView.setAdapter(new ArrayAdapter<CounterPosition>(this.getContext(),
                    android.R.layout.simple_list_item_1,
                    new CounterPositionFacade().findAll(getFilter())));
        }

    }

    private Map<String, Object> getFilter() {
        Map<String, Object> filter = new HashMap<>();
        filter.put("WorkUnit", workUnit);
        return filter;
    }

    public void setWorkUnit(WorkUnit workUnit) {
        this.workUnit = workUnit;
    }

}
