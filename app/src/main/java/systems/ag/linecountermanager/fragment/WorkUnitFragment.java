package systems.ag.linecountermanager.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import systems.ag.linecountermanager.dao.WorkUnitFacade;
import systems.ag.linecountermanager.model.WorkUnit;
import systems.ag.linecountermanager.model.WorkUnitFragmentListener;

public class WorkUnitFragment extends ListFragment {

    private WorkUnitFragmentListener listener;

    public WorkUnitFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        setListAdapter(new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_1,
                new WorkUnitFacade(context).findAll()));

        listener = (WorkUnitFragmentListener) context;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (listener != null) {
            listener.itemClicked((WorkUnit) l.getAdapter().getItem(position));
        }
    }
}
