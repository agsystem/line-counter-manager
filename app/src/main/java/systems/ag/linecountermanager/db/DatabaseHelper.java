package systems.ag.linecountermanager.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mbahbejo on 22/08/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "linecountermanager";
    private static final int DB_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null , DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE WORKUNIT (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT UNIQUE)");
        sqLiteDatabase.execSQL("CREATE TABLE COUNTERPOSITION (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "workunit TEXT, " +
                "positionnumber INTEGER, " +
                "label TEXT, " +
                "enabled NUMERIC, " +
                "active NUMERIC, " +
                "lastcount INTEGER)");
        for(int i = 1; i <= 15; i++) {
            ContentValues val = new ContentValues();
            val.put("NAME", "LINE" + i);
            sqLiteDatabase.insert("WORKUNIT", null, val);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
