package systems.ag.linecountermanager.model;

/**
 * Created by Administrator on 18/08/2016.
 */
public class CounterPosition {

    private WorkUnit workUnit;

    private int positionNumber;

    private String label;

    private String machine;

    private String description;

    private boolean enabled;

    private boolean active;

    private long lastCount;

    private LineCounterButton button;

    public WorkUnit getWorkUnit() {
        return workUnit;
    }

    public void setWorkUnit(WorkUnit workUnit) {
        this.workUnit = workUnit;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getLastCount() {
        return lastCount;
    }

    public void setLastCount(long lastCount) {
        this.lastCount = lastCount;
    }

    public LineCounterButton getButton() {
        return button;
    }

    public void setButton(LineCounterButton button) {
        this.button = button;
    }

    @Override
    public String toString() {
        return label;
    }
}
