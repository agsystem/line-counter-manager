package systems.ag.linecountermanager.model;

import java.util.List;

/**
 * Created by Administrator on 18/08/2016.
 */
public class WorkUnit {

    private String name;

    private List<CounterPosition> counterPositions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CounterPosition> getCounterPositions() {
        return counterPositions;
    }

    public void setCounterPositions(List<CounterPosition> counterPositions) {
        this.counterPositions = counterPositions;
    }

    @Override
    public String toString() {
        return name;
    }
}
