package systems.ag.linecountermanager.model;

/**
 * Created by Administrator on 18/08/2016.
 */
public class LineCounterButton {

    private CounterPosition position;

    private int inputNumber;

    public CounterPosition getPosition() {
        return position;
    }

    public void setPosition(CounterPosition position) {
        this.position = position;
    }

    public int getInputNumber() {
        return inputNumber;
    }

    public void setInputNumber(int inputNumber) {
        this.inputNumber = inputNumber;
    }
}
