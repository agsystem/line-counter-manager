package systems.ag.linecountermanager.model;

/**
 * Created by mbahbejo on 22/08/16.
 */
public interface WorkUnitFragmentListener {

    public void itemClicked(WorkUnit workUnit);
}
