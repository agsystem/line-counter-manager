package systems.ag.linecountermanager.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import systems.ag.linecountermanager.model.CounterPosition;
import systems.ag.linecountermanager.model.WorkUnit;

/**
 * Created by mbahbejo on 22/08/16.
 */
public class CounterPositionFacade {

    public List<CounterPosition> findAll(Map<String, Object> filter) {
        List<CounterPosition> counterPositions = new ArrayList<>();
        WorkUnit context = (WorkUnit) filter.get("WorkUnit");
        if(context != null) {
            for (int i = 1; i <= 40; i++) {
                CounterPosition cp = new CounterPosition();
                cp.setWorkUnit(context);
                cp.setPositionNumber(i);
                cp.setLabel(context.getName() + ":" + i);
                counterPositions.add(cp);
            }
        }
        return counterPositions;
    }

}
