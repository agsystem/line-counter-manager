package systems.ag.linecountermanager.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import systems.ag.linecountermanager.db.DatabaseHelper;
import systems.ag.linecountermanager.model.WorkUnit;

/**
 * Created by mbahbejo on 22/08/16.
 */
public class WorkUnitFacade {

    private Context context;

    public WorkUnitFacade(Context context) {
        this.context = context;
    }

    public List<WorkUnit> findAll() {
        List<WorkUnit> workUnits = new ArrayList<>();

        try {
            SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            Cursor cursor = db.query("WORKUNIT",
                    new String[] {"NAME"},
                    null,
                    null,
                    null,
                    null,
                    null);
            if(cursor.moveToFirst()) {
                do {
                    WorkUnit workUnit = new WorkUnit();
                    workUnit.setName(cursor.getString(0));
                    workUnits.add(workUnit);
                } while(cursor.moveToNext());
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e.getMessage());
        }

        return workUnits;
    }
}
